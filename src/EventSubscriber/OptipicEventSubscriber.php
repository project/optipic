<?php
namespace Drupal\optipic\EventSubscriber;

use Drupal\optipic\Optipic;
use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Component\HttpKernel\Event\FilterResponseEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Event Subscriber OptipicEventSubscriber.
 */
class OptipicEventSubscriber implements EventSubscriberInterface {

    /**
     * Code that should be triggered on event specified
     *
     * @param \Symfony\Component\HttpKernel\Event\FilterResponseEvent $event
     */
    public function onResponse(FilterResponseEvent $event) {
        /*$notAdmin = true;
        $roles = \Drupal::currentUser()->getRoles();
        foreach ($roles as $role){
            if ($role == 'administrator')
                $notAdmin = false;
        }
        if ($notAdmin) {*/
        if (\Drupal::service('router.admin_context')->isAdminRoute()==false) {
            $response = $event->getResponse();
            $content = $response->getContent();
            $optipic = new Optipic();
            $content = $optipic->changeContent($content);
            $response->setContent($content);
        }
        else {
            
            //if($sid = $settings['site_id']) { 
            //$uri = JUri::getInstance(); 
            //$host = $uri->getHost();
            
            if(\Drupal::routeMatch()->getRouteName()=='optipic.settings') {
                $optipic = new Optipic();
                $settings = $optipic->getSettings();
                $host = \Drupal::request()->getHost();
                if(!empty($host)) {
                    $sid = $settings['site_id']; 
                    $jsUrl = 'https://optipic.io/api/cp/stat?domain='.$host.'&sid='.$sid.'&cms=drupal&stype=cdn&append_to=%23optipic-settings-form%3Afirst&version=1.15.1';
                    $js = '<script type="text/javascript" src="'.$jsUrl.'"></script>';
                    
                    $response = $event->getResponse();
                    $bodyHtml = $response->getContent();
                    $bodyHtml = str_replace ("</body>", $js." </body>", $bodyHtml);
                    $response->setContent($bodyHtml);
                }
            }
        }
    }

    /**
     * {@inheritdoc}
     */
    public static function getSubscribedEvents() {
        // For this example I am using KernelEvents constants (see below a full list).
        $events[KernelEvents::RESPONSE][] = ['onResponse'];
        return $events;
    }

}