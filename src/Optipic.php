<?php
namespace Drupal\optipic;

use Drupal\optipic\ImgUrlConverter;

class Optipic {

    public function changeContent($content) {

        $settings = $this->getSettings();

        if($settings['autoreplace_active'] && $settings['site_id']) {
            ImgUrlConverter::loadConfig($settings);
            $content = ImgUrlConverter::convertHtml($content);
        }

        return $content;
    }

    public function getSettings() {

        $config = \Drupal::config('optipic.settings');

        $autoreplaceActive = $config->get('autoreplace_active');
        $siteId = $config->get('site_id');
        $domains = $config->get('domains')!=''? explode("\n", $config->get('domains')):array();
        $exclusionsUrl = $config->get('exclusions_url')!=''?explode("\n", $config->get('exclusions_url')):array();
        $whitelistImgUrls = $config->get('whitelist_img_urls')!=''?explode("\n", $config->get('whitelist_img_urls')):array();
        $srcsetAttrs = $config->get('srcset_attrs')!=''?explode("\n", $config->get('srcset_attrs')):array();

        return array(
            'autoreplace_active' => $autoreplaceActive,
            'site_id' => $siteId,     // your SITE ID from CDN OptiPic controll panel
            'domains' => $domains,  // list of domains should replace to cdn.optipic.io
            'exclusions_url' => $exclusionsUrl, // list of URL exclusions - where is URL should not converted
            'whitelist_img_urls' => $whitelistImgUrls, // whitelist of images URL - what should to be converted (parts or full urls start from '/')
            'srcset_attrs' => $srcsetAttrs, // tag's srcset attributes // @see https://developer.mozilla.org/en-US/docs/Learn/HTML/Multimedia_and_embedding/Responsive_images
        );
    }
}