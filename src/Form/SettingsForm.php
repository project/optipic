<?php
namespace Drupal\optipic\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\optipic\ImgUrlConverter;

class SettingsForm extends ConfigFormBase {

    /**
     * Gets the configuration names that will be editable.
     *
     * @return array
     *   An array of configuration object names that are editable if called in
     *   conjunction with the trait's config() method.
     */
    protected function getEditableConfigNames()
    {
        return ['optipic.settings'];
    }

    /**
     * Returns a unique string identifying the form.
     *
     * The returned ID should be a unique string that can be a valid PHP function
     * name, since it's used in hook implementation names such as
     * hook_form_FORM_ID_alter().
     *
     * @return string
     *   The unique string identifying the form.
     */
    public function getFormId()
    {
        return 'optipic_settings_form';
    }

    /**
     * {@inheritdoc}
     */
    public function buildForm(array $form, FormStateInterface $form_state) {

        // Form constructor.
        $form = parent::buildForm($form, $form_state);
        // Default settings.
        $config = $this->config('optipic.settings');
        
        $defaultSettings = ImgUrlConverter::getDefaultSettings();
        
        $defaultDomains = (!empty($config->get('domains')))? $config->get('domains'): implode("\n", $defaultSettings['domains']);
        
        // autoreplace_active field.
        $form['autoreplace_active'] = array(
            '#type' => 'checkbox',
            '#title' => $this->t('Enable auto-replace image URLs'),
            '#default_value' => $config->get('autoreplace_active')
        );
        // site_id field.
        $form['site_id'] = array(
            '#type' => 'textfield',
            '#title' => $this->t('Site ID in your CDN OptiPic account'),
            '#default_value' => $config->get('site_id')
        );
        // domains field.
        $form['domains'] = array(
            '#type' => 'textarea',
            '#title' => $this->t('Domain list (if images are loaded via absolute URLs)'),
            '#default_value' => $defaultDomains
        );
        // exclusions_url field.
        $form['exclusions_url'] = array(
            '#type' => 'textarea',
            '#title' => $this->t('Site pages that do not include auto-change'),
            '#default_value' => $config->get('exclusions_url')
        );
        // whitelist_img_urls field.
        $form['whitelist_img_urls'] = array(
            '#type' => 'textarea',
            '#title' => $this->t('Replace only URLs of images starting with a mask'),
            '#default_value' => $config->get('whitelist_img_urls')
        );
        // srcset_attrs field.
        $form['srcset_attrs'] = array(
            '#type' => 'textarea',
            '#title' => $this->t("List of 'srcset' attributes"),
            '#default_value' => $config->get('srcset_attrs')
        );
        
        return $form;
    }

    /**
     * {@inheritdoc}
     */
    public function validateForm(array &$form, FormStateInterface $form_state) {
        parent::validateForm($form, $form_state);
        /*Let active plugins validate their settings.
        foreach ($this->configurableInstances as $instance) {
            $instance->validateConfigurationForm($form, $form_state);
        }*/
    }

    /**
     * {@inheritdoc}
     */
    public function submitForm(array &$form, FormStateInterface $form_state) {
        parent::submitForm($form, $form_state);
        $config = $this->config('optipic.settings');
        /* Let active plugins save their settings.
        foreach ($this->configurableInstances as $instance) {
            $instance->submitConfigurationForm($form, $form_state);
        }*/

        if ($form_state->hasValue('autoreplace_active')) {
            $config->set('autoreplace_active', $form_state->getValue('autoreplace_active'));
        }
        if ($form_state->hasValue('site_id')) {
            $config->set('site_id', $form_state->getValue('site_id'));
        }
        if ($form_state->hasValue('domains')) {
            $config->set('domains', $form_state->getValue('domains'));
        }
        if ($form_state->hasValue('exclusions_url')) {
            $config->set('exclusions_url', $form_state->getValue('exclusions_url'));
        }
        if ($form_state->hasValue('whitelist_img_urls')) {
            $config->set('whitelist_img_urls', $form_state->getValue('whitelist_img_urls'));
        }
        if ($form_state->hasValue('srcset_attrs')) {
            $config->set('srcset_attrs', $form_state->getValue('srcset_attrs'));
        }
        $config->save();
    }
}